Concernant la methodologie utilis�e:

J'avais �tudi� l'analyse de l'existant l'ann�e derni�re de ce m�me, ceci etant fait j'ai pu me consacrer � l'analyse fonctionnel de la solution qui est resortie du travail pr�cedant. Cette version n'etant pas complete, je souhaitais repartir du d�but, recommancer l'analyse fonctionnel. Cela me permetrait de clarifier mon travail et de l'am�liorer. En effet, le d�but de mon analyse n'est plus � jour ni ne concorde avec ma pens�e qui a naturellement �volu� au fil de mon travail.

Cette reflexion me fait penser � la methodologie Agile, qui quand on aime bien l'analyse peu convenir en tout cas � mon projet. Je ressend en effet le besoin d'avoir plus de souplesse dans mon travail tout en respectant le besoin de mon client/promoteur interne. 

Je vais maintenant le lui pr�senter et il va tr�s probablement y apporter des mises � jour �galement. Je serais alors pret pour faire une r�vision de l'analyse afin de continuer et d'affiner ce travail.

Bonne Lecture.