USE [Mon_Cabinet]
GO
/****** Object:  StoredProcedure [dbo].[spGenerateDBDictionary]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[spGenerateDBDictionary] 
AS
BEGIN

select a.name [Table],b.name [Attribute],c.name [DataType],b.isnullable [Allow Nulls?],CASE WHEN 
d.name is null THEN 0 ELSE 1 END [PKey?],
CASE WHEN e.parent_object_id is null THEN 0 ELSE 1 END [FKey?],CASE WHEN e.parent_object_id 
is null THEN '-' ELSE g.name  END [Ref Table],
CASE WHEN h.value is null THEN '-' ELSE h.value END [Description]
from sysobjects as a
join syscolumns as b on a.id = b.id
join systypes as c on b.xtype = c.xtype 
left join (SELECT  so.id,sc.colid,sc.name 
      FROM    syscolumns sc
      JOIN sysobjects so ON so.id = sc.id
      JOIN sysindexkeys si ON so.id = si.id 
                    and sc.colid = si.colid
      WHERE si.indid = 1) d on a.id = d.id and b.colid = d.colid
left join sys.foreign_key_columns as e on a.id = e.parent_object_id and b.colid = e.parent_column_id    
left join sys.objects as g on e.referenced_object_id = g.object_id  
left join sys.extended_properties as h on a.id = h.major_id and b.colid = h.minor_id
where a.type = 'U' order by a.name

END
GO
/****** Object:  Table [dbo].[Medecin]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Medecin](
	[Adresse_cabinet] [varchar](30) NULL,
	[ID_Medecin] [uniqueidentifier] NOT NULL,
	[FK_Medecin] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Medecin_1] PRIMARY KEY CLUSTERED 
(
	[ID_Medecin] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[O_En_attente]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[O_En_attente](
	[FK_O_En_attente] [uniqueidentifier] NOT NULL,
 CONSTRAINT [ID_O_En_attente] UNIQUE NONCLUSTERED 
(
	[FK_O_En_attente] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[O_Refusee]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[O_Refusee](
	[Raison] [varchar](30) NULL,
	[Status_envoi_confirmation] [tinyint] NOT NULL,
	[FK_O_Refusee] [uniqueidentifier] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[O_Validee]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[O_Validee](
	[Lieux_de_récupération] [varchar](30) NOT NULL,
	[Status_envoi_confirmation] [tinyint] NOT NULL,
	[FK_O_Validee] [uniqueidentifier] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ordonnance]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ordonnance](
	[ID_ordonnance] [uniqueidentifier] NOT NULL,
	[Nom_medicament] [varchar](15) NOT NULL,
	[Date] [timestamp] NOT NULL,
	[FK_Patient] [uniqueidentifier] NOT NULL,
	[FK_Medecin] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Ordonnance] PRIMARY KEY CLUSTERED 
(
	[ID_ordonnance] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY],
 CONSTRAINT [IX_Ordonnance] UNIQUE NONCLUSTERED 
(
	[ID_ordonnance] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Patient]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Patient](
	[Nombre_demande_ordonnance] [int] NOT NULL,
	[Nombre_demande_rendez-vous] [int] NOT NULL,
	[ID_Patient] [uniqueidentifier] NOT NULL,
	[FK_Patient] [uniqueidentifier] NOT NULL,
 CONSTRAINT [PK_Patient] PRIMARY KEY CLUSTERED 
(
	[ID_Patient] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Personne]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Personne](
	[ID_Personne] [uniqueidentifier] NOT NULL,
	[Nom] [varchar](15) NOT NULL,
	[Prenom] [varchar](15) NOT NULL,
	[Numero_Gsm] [int] NOT NULL,
 CONSTRAINT [PK_Personne_1] PRIMARY KEY CLUSTERED 
(
	[ID_Personne] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PH_Confirmee]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PH_Confirmee](
	[Statut_envoi_confirmation] [tinyint] NOT NULL,
	[FK_PH_Confirmee] [uniqueidentifier] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PH_Disponible]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PH_Disponible](
	[Depuis_quand] [timestamp] NOT NULL,
	[FK_PH_Disponible] [uniqueidentifier] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PH_En_attente]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PH_En_attente](
	[FK_PH_En_attente] [uniqueidentifier] NOT NULL,
	[Date_de_la_demande] [timestamp] NOT NULL
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[PH_Indisponible]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PH_Indisponible](
	[Raison] [varchar](150) NULL,
	[FK_PH_Indisponible] [uniqueidentifier] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PH_Refusee]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PH_Refusee](
	[Statut_envoi_confirmation] [tinyint] NOT NULL,
	[Raison] [varchar](150) NULL,
	[FK_PH_Refusee] [uniqueidentifier] NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Plage_Horaire]    Script Date: 03-06-16 15:21:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Plage_Horaire](
	[ID_plage_horaire] [uniqueidentifier] NOT NULL,
	[FK_Patient] [uniqueidentifier] NOT NULL,
	[FK_Medecin] [uniqueidentifier] NOT NULL,
	[Date] [timestamp] NOT NULL,
 CONSTRAINT [PK_Plage_Horaire] PRIMARY KEY CLUSTERED 
(
	[ID_plage_horaire] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
ALTER TABLE [dbo].[Medecin]  WITH CHECK ADD  CONSTRAINT [FK_Medecin_Personne] FOREIGN KEY([FK_Medecin])
REFERENCES [dbo].[Personne] ([ID_Personne])
GO
ALTER TABLE [dbo].[Medecin] CHECK CONSTRAINT [FK_Medecin_Personne]
GO
ALTER TABLE [dbo].[O_En_attente]  WITH CHECK ADD  CONSTRAINT [FK_O_En_attente_Ordonnance] FOREIGN KEY([FK_O_En_attente])
REFERENCES [dbo].[Ordonnance] ([ID_ordonnance])
GO
ALTER TABLE [dbo].[O_En_attente] CHECK CONSTRAINT [FK_O_En_attente_Ordonnance]
GO
ALTER TABLE [dbo].[O_Refusee]  WITH CHECK ADD  CONSTRAINT [FK_O_Refusee_Ordonnance] FOREIGN KEY([FK_O_Refusee])
REFERENCES [dbo].[Ordonnance] ([ID_ordonnance])
GO
ALTER TABLE [dbo].[O_Refusee] CHECK CONSTRAINT [FK_O_Refusee_Ordonnance]
GO
ALTER TABLE [dbo].[O_Validee]  WITH CHECK ADD  CONSTRAINT [FK_O_Validee_Ordonnance] FOREIGN KEY([FK_O_Validee])
REFERENCES [dbo].[Ordonnance] ([ID_ordonnance])
GO
ALTER TABLE [dbo].[O_Validee] CHECK CONSTRAINT [FK_O_Validee_Ordonnance]
GO
ALTER TABLE [dbo].[Ordonnance]  WITH CHECK ADD  CONSTRAINT [FK_Ordonnance_Medecin] FOREIGN KEY([FK_Medecin])
REFERENCES [dbo].[Medecin] ([ID_Medecin])
GO
ALTER TABLE [dbo].[Ordonnance] CHECK CONSTRAINT [FK_Ordonnance_Medecin]
GO
ALTER TABLE [dbo].[Ordonnance]  WITH CHECK ADD  CONSTRAINT [FK_Ordonnance_Patient] FOREIGN KEY([FK_Patient])
REFERENCES [dbo].[Patient] ([ID_Patient])
GO
ALTER TABLE [dbo].[Ordonnance] CHECK CONSTRAINT [FK_Ordonnance_Patient]
GO
ALTER TABLE [dbo].[Patient]  WITH CHECK ADD  CONSTRAINT [FK_Patient_Personne1] FOREIGN KEY([FK_Patient])
REFERENCES [dbo].[Personne] ([ID_Personne])
GO
ALTER TABLE [dbo].[Patient] CHECK CONSTRAINT [FK_Patient_Personne1]
GO
ALTER TABLE [dbo].[PH_Confirmee]  WITH CHECK ADD  CONSTRAINT [FK_PH_Confirmee_Plage_Horaire1] FOREIGN KEY([FK_PH_Confirmee])
REFERENCES [dbo].[Plage_Horaire] ([ID_plage_horaire])
GO
ALTER TABLE [dbo].[PH_Confirmee] CHECK CONSTRAINT [FK_PH_Confirmee_Plage_Horaire1]
GO
ALTER TABLE [dbo].[PH_Disponible]  WITH CHECK ADD  CONSTRAINT [FK_PH_Disponible_Plage_Horaire1] FOREIGN KEY([FK_PH_Disponible])
REFERENCES [dbo].[Plage_Horaire] ([ID_plage_horaire])
GO
ALTER TABLE [dbo].[PH_Disponible] CHECK CONSTRAINT [FK_PH_Disponible_Plage_Horaire1]
GO
ALTER TABLE [dbo].[PH_En_attente]  WITH CHECK ADD  CONSTRAINT [FK_PH_En_attente_Plage_Horaire1] FOREIGN KEY([FK_PH_En_attente])
REFERENCES [dbo].[Plage_Horaire] ([ID_plage_horaire])
GO
ALTER TABLE [dbo].[PH_En_attente] CHECK CONSTRAINT [FK_PH_En_attente_Plage_Horaire1]
GO
ALTER TABLE [dbo].[PH_Indisponible]  WITH CHECK ADD  CONSTRAINT [FK_PH_Indisponible_Plage_Horaire1] FOREIGN KEY([FK_PH_Indisponible])
REFERENCES [dbo].[Plage_Horaire] ([ID_plage_horaire])
GO
ALTER TABLE [dbo].[PH_Indisponible] CHECK CONSTRAINT [FK_PH_Indisponible_Plage_Horaire1]
GO
ALTER TABLE [dbo].[PH_Refusee]  WITH CHECK ADD  CONSTRAINT [FK_PH_Refusee_Plage_Horaire1] FOREIGN KEY([FK_PH_Refusee])
REFERENCES [dbo].[Plage_Horaire] ([ID_plage_horaire])
GO
ALTER TABLE [dbo].[PH_Refusee] CHECK CONSTRAINT [FK_PH_Refusee_Plage_Horaire1]
GO
ALTER TABLE [dbo].[Plage_Horaire]  WITH CHECK ADD  CONSTRAINT [FK_Plage_Horaire_Medecin] FOREIGN KEY([FK_Medecin])
REFERENCES [dbo].[Medecin] ([ID_Medecin])
GO
ALTER TABLE [dbo].[Plage_Horaire] CHECK CONSTRAINT [FK_Plage_Horaire_Medecin]
GO
ALTER TABLE [dbo].[Plage_Horaire]  WITH CHECK ADD  CONSTRAINT [FK_Plage_Horaire_Patient] FOREIGN KEY([FK_Patient])
REFERENCES [dbo].[Patient] ([ID_Patient])
GO
ALTER TABLE [dbo].[Plage_Horaire] CHECK CONSTRAINT [FK_Plage_Horaire_Patient]
GO
